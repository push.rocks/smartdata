/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@pushrocks/smartdata',
  version: '5.0.20',
  description: 'do more with data'
}
