import * as plugins from './smartdata.plugins.js';
import { Collection } from './smartdata.classes.collection.js';
import { SmartdataDb } from './smartdata.classes.db.js';
import { SmartDataDbDoc, svDb, unI } from './smartdata.classes.doc.js';

/**
 * EasyStore allows the storage of easy objects. It also allows easy sharing of the object between different instances
 */
export class EasyStore<T> {
  // instance
  public smartdataDbRef: SmartdataDb;
  public nameId: string;

  private easyStoreClass = (() => {
    @Collection(() => this.smartdataDbRef)
    class SmartdataEasyStore extends SmartDataDbDoc<SmartdataEasyStore, SmartdataEasyStore> {
      @unI()
      public nameId: string;

      @svDb()
      public ephermal: {
        activated: boolean;
        timeout: number;
      };

      @svDb()
      lastEdit: number;

      @svDb()
      public data: Partial<T>;
    }
    return SmartdataEasyStore;
  })();

  constructor(nameIdArg: string, smnartdataDbRefArg: SmartdataDb) {
    this.smartdataDbRef = smnartdataDbRefArg;
    this.nameId = nameIdArg;
  }

  private async getEasyStore() {
    let easyStore = await this.easyStoreClass.getInstance({
      nameId: this.nameId,
    });

    if (!easyStore) {
      easyStore = new this.easyStoreClass();
      easyStore.nameId = this.nameId;
      easyStore.data = {};
      await easyStore.save();
    }
    return easyStore;
  }

  /**
   * reads all keyValue pairs at once and returns them
   */
  public async readAll() {
    const easyStore = await this.getEasyStore();
    return easyStore.data;
  }

  /**
   * reads a keyValueFile from disk
   */
  public async readKey(keyArg: keyof T) {
    const easyStore = await this.getEasyStore();
    return easyStore.data[keyArg];
  }

  /**
   * writes a specific key to the keyValueStore
   */
  public async writeKey(keyArg: keyof T, valueArg: any) {
    const easyStore = await this.getEasyStore();
    easyStore.data[keyArg] = valueArg;
    await easyStore.save();
  }

  public async deleteKey(keyArg: keyof T) {
    const easyStore = await this.getEasyStore();
    delete easyStore.data[keyArg];
    await easyStore.save();
  }

  /**
   * writes all keyValue pairs in the object argument
   */
  public async writeAll(keyValueObject: Partial<T>) {
    const easyStore = await this.getEasyStore();
    easyStore.data = { ...easyStore.data, ...keyValueObject };
    await easyStore.save();
  }

  /**
   * wipes a key value store from disk
   */
  public async wipe() {
    const easyStore = await this.getEasyStore();
    easyStore.data = {};
    await easyStore.save();
  }

  public async cleanUpEphermal() {
    while (
      (await this.smartdataDbRef.statusConnectedDeferred.promise) &&
      this.smartdataDbRef.status === 'connected'
    ) {}
  }
}
