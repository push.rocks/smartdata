import * as plugins from './smartdata.plugins.js';

export const getNewUniqueId = async (prefixArg?: string) => {
  return plugins.smartunique.uni(prefixArg);
};
